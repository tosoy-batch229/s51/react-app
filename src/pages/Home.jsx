import React from "react";
import Banner from "../components/Banner.jsx";
import Highlights from "../components/Highlights.jsx";
import { Container } from "react-bootstrap";

export default function Home() {
    return (
        <React.Fragment>
            <Container>
                <Banner/>
                <Highlights/>
            </Container>
        </React.Fragment>
    )
}