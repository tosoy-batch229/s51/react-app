import React from 'react';
import { Row, Card, Button } from 'react-bootstrap';
import { useState } from 'react';

export default function CourseCard({courseProp}) {
    // Check if the data is successfully passed.
    console.log(courseProp);
    console.log(typeof courseProp);

    // Destructure the data to avoid dot notation.
    const { name, description, price } = courseProp;

    // 3 Hooks in React
    // 1. useState
    // 2. useEffect
    // 3. useContext

    // Use the useState hook for the component to be able to store state.
    // States are used to keep track of important related to individual components.
    // Syntax -> const [getter, setter] = useState(0);
    const [count, setCount] = useState(0);
    const [seat, setSeat] = useState(30);

    function enroll() {
        if (seat === 0) {
            alert('No more seats left');
        } else {
            setCount(count + 1);
            setSeat(seat - 1);
        }
    };

    return (
        <React.Fragment>
            <Row className="mt-4 mx-auto">
                <Card className='px-0 my-3'>
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Text>
                            <h6>Description:</h6>
                            {description}
                        </Card.Text>
                        <Card.Text>
                            <h6>Price: </h6>
                            {price}
                        </Card.Text>

                        <Card.Text>
                            Enrollees: {count}<br></br>
                            Seats: {seat}
                        </Card.Text>
                        <Button onClick={enroll} variant="primary">Enroll!</Button>
                    </Card.Body>
                </Card>
            </Row>
        </React.Fragment>
    )
}