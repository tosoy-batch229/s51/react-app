import React from "react";
import { Container } from "react-bootstrap";
import "./App.css";

import AppNavbar from "./components/AppNavbar.jsx";
import Home from './pages/Home.jsx'
import Courses from "./pages/Courses.jsx";

function App() {
    return (
        <React.Fragment>
            <AppNavbar />
            <Container>
                <Home/>
                <Courses/>
            </Container>
        </React.Fragment>
    );
}

export default App;